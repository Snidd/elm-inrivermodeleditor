module Cvl.View exposing (..)

import Html exposing (..)
import Html.Attributes exposing (class, value, href)
import Msgs exposing (Msg)
import Models exposing (EntityType, Model, CvlId)

view : Model -> CvlId -> Html Msg
view model id =
    div []
        [ text "specific cvl"
        ]
name : String
name = 
    "CVLs"

path : String
path =
    "cvls"