
module Routing exposing (..)

import Navigation exposing (Location)
import Models exposing (EntityTypeId, Route(..))
import UrlParser exposing (..)
import EntityType.View
import EntityTypes.View
import Cvls.View

matchers : Parser (Route -> a) a
matchers =
    oneOf
        [ map EntityTypesRoute top
        , map EntityTypeRoute (s EntityType.View.path </> string)
        , map EntityTypesRoute (s EntityTypes.View.path)
        , map CvlsRoute (s Cvls.View.path)
        , map CvlRoute (s Cvls.View.path </> string)
        ]


parseLocation : Location -> Route
parseLocation location =
    case (parseHash matchers location) of
        Just route ->
            route

        Nothing ->
            NotFoundRoute