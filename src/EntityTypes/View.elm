module EntityTypes.View exposing (..)

import Html exposing (..)
import Html.Attributes exposing (class, value, href, attribute)
import Msgs exposing (Msg)
import Models exposing (Model, InRiverTypes, EntityType)
import Dict

view : Maybe (List EntityType)  -> Html Msg
view maybeEntityTypes =
    case maybeEntityTypes of
        Nothing ->
            text ""
        Just entityTypes ->
            div [ class "columns is-multiline" ]
                (List.map getEntityTypeCard entityTypes)

name : String
name = 
    "EntityTypes"

path : String
path =
    "entitytypes"

getEntityTypeCard : EntityType -> Html Msg
getEntityTypeCard entityType =
    div [ class "column is-quarter" ] [
        div [ class "card" ] [
            header [ class "card-header" ] [
                p [ class "card-header-title" ] [
                    text entityType.id
                ],
                a [ href "#", class "card-header-icon", attribute "aria-label" "more options" ] [
                    span [ class "icon" ] [
                        i [ class "fa fa-angle-down", attribute "aria-hidden" "true" ] []
                    ]
                ]
            ],
            div [ class "card-content" ] [
                div [ class "content" ] [
                    text "Lorem ipsum"
                ]
            ],
            footer [ class "card-footer" ] [
                a [ href "#", class "card-footer-item" ] [
                    text "Edit"
                ],
                a [ href "#", class "card-footer-item" ] [
                    text "Delete"
                ]
            ]
        ]
    ]