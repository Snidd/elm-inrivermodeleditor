module Cvls.View exposing (..)

import Html exposing (..)
import Html.Attributes exposing (class, value, href)
import Msgs exposing (Msg)
import Models exposing (Model)

view : Model -> Html Msg
view model =
    div []
        [ text "cvls"
        ]

name : String
name = 
    "CVLs"

path : String
path =
    "cvls"