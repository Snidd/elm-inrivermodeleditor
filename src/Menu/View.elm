module Menu.View exposing (mainMenu)

import Html exposing (Html, div, aside, ul, section, li, a, p, text)
import Html.Attributes exposing (class, href)
import Models exposing (Model, EntityType, InRiverTypes, Route)
import EntityTypes.View
import Cvls.View
import Routing exposing (parseLocation)
import Navigation exposing (Location)

mainMenu : Model -> Html msg
mainMenu model =
    aside [ class "menu", class "is-size-7" ]
        [ p [ class "menu-label" ]
            [ text "Marketing Model" ]
        , ul [ class "menu-list" ]
            [ 
                parentMenuOption 1 EntityTypes.View.name EntityTypes.View.path (getInRiverTypeFromEntity (model.entityTypes)) model,
                parentMenuOption 2 Cvls.View.name Cvls.View.path (getInRiverTypeFromCvl (model.cvls)) model
            ]
        ]

getInRiverTypeFromEntity : Maybe (List EntityType) -> Maybe (List InRiverTypes)
getInRiverTypeFromEntity entity =
    case entity of
        Nothing ->
            Nothing
        Just entities ->
            Just (List.map Models.InRiverEntity entities)

getInRiverTypeFromCvl : Maybe (List Models.Cvl) -> Maybe (List InRiverTypes)
getInRiverTypeFromCvl cvl =
    case cvl of
        Nothing ->
            Nothing
        Just cvls ->
            Just (List.map Models.InRiverCvl cvls)

testActive : String -> Route -> Bool
testActive path currentRoute =
    if (parseLocation (getLocationFromString path)) == currentRoute then
        True
    else
        False

menuOption : String -> Route -> InRiverTypes -> Html msg
menuOption parentPath currentRoute entityType =
    li []
        [ a [ 
                href ("#" ++ parentPath ++ "/" ++ (Models.getInRiverId entityType)), 
                isActive (parentPath ++ "/" ++ (Models.getInRiverId entityType)) currentRoute
            ] 
            [ text ((Models.getInRiverId entityType)) ]
        ]

getLocationFromString : String -> Location
getLocationFromString path =
    (Location path "" "" "http" "" "" ("/" ++ path) "" ("#" ++ path) "" "")


parentMenuOption : Int -> String -> String -> (Maybe (List InRiverTypes)) -> Model -> Html msg
parentMenuOption counter menuName menuPath subMenus model =
    case subMenus of
        Nothing ->
            text ""
        Just existingTypes ->
            li []
                [ a [ (isActive menuPath model.route), href ("#" ++ menuPath) ] 
                    [ text ((toString counter) ++ ". " ++ menuName) ]
                , ul [] (List.map (menuOption menuPath model.route) existingTypes)
                ]

isActive : String -> Route -> Html.Attribute msg
isActive menuPath currentRoute =
    if (parseLocation (getLocationFromString menuPath)) == currentRoute then
        class "is-active"
    else
        class ""