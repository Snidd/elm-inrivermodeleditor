module Breadcrumb.View exposing (..)

import Html exposing (li, a, Html, nav, text, ul)
import Html.Attributes exposing (class, attribute, value, href)
import Msgs exposing (Msg)
import Models exposing (Route)
import List exposing (map)
import EntityTypes.View
import EntityType.View
import Cvls.View


view : Route -> Html Msg
view route =
    case route of
        Models.EntityTypesRoute ->
            getBreadcrumbNav [ getBreadcrumbRoot, (BreadcrumbItem EntityTypes.View.name EntityTypes.View.path True) ]
        Models.EntityTypeRoute id ->
            getBreadcrumbNav [ 
                getBreadcrumbRoot,
                (BreadcrumbItem EntityTypes.View.name EntityTypes.View.path False), 
                (BreadcrumbItem id (EntityTypes.View.path ++ "/" ++ id) True)
            ]
        Models.CvlsRoute ->
            getBreadcrumbNav [ getBreadcrumbRoot, (BreadcrumbItem Cvls.View.name Cvls.View.path True) ]
        Models.CvlRoute id ->
            getBreadcrumbNav [ 
                getBreadcrumbRoot,
                (BreadcrumbItem Cvls.View.name Cvls.View.path False), 
                (BreadcrumbItem id (Cvls.View.path ++ "/" ++ id) True)
            ]
        Models.NotFoundRoute ->
            getBreadcrumbNav [(BreadcrumbItem "404" "/" True)]

getBreadcrumbRoot : BreadcrumbItem 
getBreadcrumbRoot =
    (BreadcrumbItem "IIME" "/" False)

getBreadcrumbNav : List BreadcrumbItem -> Html Msg
getBreadcrumbNav pages =
    nav [ class "breadcrumb", attribute "aria-label" "breadcrumbs" ]
        [ ul []
            (List.map getBreadcrumbLi pages)
        ]
        

getBreadcrumbLi : BreadcrumbItem -> Html Msg
getBreadcrumbLi item =
    li [ getActiveClass item.isActive ]
        [ a [ href ("#" ++ item.link) ]
            [ text item.name ]
        ]


getActiveClass : Bool -> Html.Attribute msg
getActiveClass active =
    if active then
        class "is-active"
    else
        class ""


type alias BreadcrumbItem =
    { name: String,
      link : String,
      isActive : Bool
    }