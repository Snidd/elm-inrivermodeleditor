module Notifications.View exposing (..)

import Html exposing (li, a, div, button, section, Html, nav, text, ul)
import Html.Attributes exposing (class, attribute, value, href)
import Msgs exposing (Msg)
import Models exposing (NotificationMessage)
import Html.Events exposing (onClick)



view : Maybe (List NotificationMessage) -> Html Msg
view maybeMessages =
    case maybeMessages of 
        Nothing ->
            text ""
        Just messages ->
            if List.length messages > 0 then
                section [ class "section" ] (List.map drawMessage messages)
            else
                text ""
            

drawMessage : NotificationMessage -> Html Msg
drawMessage message =
    div [ getNotificationClass message.notificationType ]
        [ button [ class "delete", onClick (Msgs.RemoveNotification message.id) ]
            []
        , text message.message
        ]

getNotificationClass : Models.NotificationType -> Html.Attribute msg
getNotificationClass notificationType =
    case notificationType of
        Models.Normal ->
            class "notification"
        Models.Primary ->
            class "notification is-primary"
        Models.Link ->
            class "notification is-link"
        Models.Info ->
            class "notification is-info"
        Models.Success ->
            class "notification is-success"
        Models.Warning ->
            class "notification is-warning"
        Models.Danger ->
            class "notification is-danger"