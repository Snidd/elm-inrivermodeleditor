module Msgs exposing (..)

import Models exposing (NotificationId)
import Navigation exposing (Location)


type Msg
    = OnLocationChange Location 
    | RemoveNotification NotificationId 
    | NoOp