module Models exposing (..)
import Dict exposing (..)

type alias Model =
    { entityTypes : Maybe (List EntityType),
      cvls : Maybe (List Cvl),
      route: Route,
      notifications: Maybe (List NotificationMessage)
    }

initialModel : Route -> Model
initialModel route =
    {
        entityTypes = Just [
            EntityType "Product" (Dict.fromList([ ("en", "Product"), ("sv", "Produkt") ])) False,
            EntityType "Article" (Dict.fromList([ ("en", "Article"), ("sv", "Artikel") ])) False
        ],
        cvls = Just [
            Cvl "ProductType" String Nothing False False
        ],
        route = route,
        notifications = Just [
            NotificationMessage "id1" Info "Testing",
            NotificationMessage "id2" Warning "Warning Warning!",
            NotificationMessage "id3" Danger "Danger Will Robinson!"
        ]
    }


type alias EntityTypeId =
    String

type alias CvlId =
    String

type alias LanguageKey =
    String

type alias Value =
    String

type alias StringMap =
    (Dict LanguageKey Value)

type alias EntityType =
    { id : EntityTypeId
    , name : StringMap
    , isLinkEntityType : Bool
    }

type alias Cvl =
    { id : CvlId 
    , dataType : CvlDataType
    , parentId : Maybe CvlId
    , customValueList : Bool
    , activated : Bool
    }

type alias NotificationId =
    String

type alias NotificationMessage =
    {
        id: NotificationId,
        notificationType: NotificationType,
        message: String
    }

type NotificationType =
    Normal |
    Primary |
    Link |
    Info |
    Success |
    Warning |
    Danger

type CvlDataType = String | LocaleString

type InRiverTypes = InRiverCvl Cvl | InRiverEntity EntityType

getEntityTypes : List InRiverTypes -> List (Maybe EntityType)
getEntityTypes inRiverTypes =
    List.map getEntityType inRiverTypes 

getEntityType : InRiverTypes -> Maybe EntityType
getEntityType inriverType =
    case inriverType of
        InRiverEntity ent ->
            Just ent
        InRiverCvl cvl ->
            Nothing
            

getInRiverId : InRiverTypes -> String
getInRiverId currentType =
    case currentType of
        InRiverCvl cvl ->
            cvl.id
        InRiverEntity entity ->
            entity.id

type Route
    = EntityTypesRoute
    | EntityTypeRoute EntityTypeId
    | CvlsRoute
    | CvlRoute CvlId
    | NotFoundRoute
