module View exposing (..)

import Html exposing (Html, div, aside, ul, img, button, span, section, nav, li, a, p, text)
import Html.Attributes exposing (class, href, alt, src, attribute)
import Msgs exposing (Msg)
import Models exposing (Model, EntityType, InRiverTypes, Route)
import Menu.View
import EntityTypes.View
import EntityType.View
import Breadcrumb.View
import Cvls.View
import Cvl.View
import Notifications.View

view : Model -> Html Msg
view model =
    div [ class "container" ]
        [ 
            navBar,
            Breadcrumb.View.view model.route,
            Notifications.View.view model.notifications,
            div [ class "columns"] 
            [ div [ class "column", class "is-one-quarter" ]        
                [ Menu.View.mainMenu model ],
            div [ class "column is-multiline" ]
                [ page model ]
            ]
        ]

page : Model -> Html Msg
page model =
    case model.route of
        Models.EntityTypesRoute ->
            EntityTypes.View.view model.entityTypes
        Models.EntityTypeRoute id ->
            EntityType.View.view model id
        Models.CvlsRoute ->
            Cvls.View.view model
        Models.CvlRoute id ->
            Cvl.View.view model id
        Models.NotFoundRoute ->
            notFoundView

notFoundView : Html msg
notFoundView =
    div []
        [ text "Not found"
        ]

navBar : Html Msg
navBar =
    nav [ attribute "aria-label" "main navigation", class "navbar", attribute "role" "navigation" ]
        [ div [ class "navbar-brand" ]
            [ a [ class "navbar-item", href "https://bulma.io" ]
                [ img [ alt "Bulma: a modern CSS framework based on Flexbox", attribute "height" "28", src "https://bulma.io/images/bulma-logo.png", attribute "width" "112" ]
                    []
                ]
            , button [ class "button navbar-burger" ]
                [ span []
                    []
                , span []
                    []
                , span []
                    []
                ]
            ]
        ]