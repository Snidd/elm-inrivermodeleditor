module Update exposing (..)

import Msgs exposing (Msg(..))
import Models exposing (Model, NotificationMessage,NotificationId)
import Routing exposing (parseLocation)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )
        Msgs.OnLocationChange location ->
            let
                newRoute =
                    parseLocation location
            in
                ( { model | route = newRoute }, Cmd.none )
        Msgs.RemoveNotification id ->
            case model.notifications of
                Nothing ->
                    ( model, Cmd.none )
                Just availableNotifications ->
                    let
                        newNotifications =
                            Just (List.filter (doesntEqualId id) availableNotifications)
                    in
                        
                    ( { model | notifications = newNotifications }, Cmd.none)

doesntEqualId : NotificationId -> NotificationMessage -> Bool
doesntEqualId id msg =
    if msg.id == id then
        False
    else
        True